FRE QW COOP v0.80

Requires mvdsv_xe or mvdsv <= 0.28 or ezquake 2.1

mvdsv_xe highly recommended since it has extended entity limits required for large maps.

Newer version of mvdsv *WON'T* work due to mvdsv removing the "mvdsv qc extensions"... yes, they actually removed their own extensions, conveniently breaking FODFFA, the most popular QW mod, among others.

Don't develop for MVDSV. Just read the source to FODFFA if you need another warning.

License - GPL

QuakeC by freewill.

Checkpoints, coop spawns, and testing by [HCI]Maraakate.

Based on "quakeworld undergate qc" by Baker, RocketGuy and Lardarse.

Most of the undergate QC (about 200 lines of code) has been removed and about 5000+ lines of new code has been added.   