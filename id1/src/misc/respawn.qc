//==========================================================================
//  respawn.qc -- by Patrick Martin             Last updated:  9-11-1997
//--------------------------------------------------------------------------
//  This file contains code that lets monsters respawn back to life.
//==========================================================================

//--------------------//
//  NEW EDICT FIELDS  //
//--------------------//
.vector  oldangles;     // Monster bearings at the start of the level.
.vector  oldstart;      // Monster location at the start of the level.
.float   oncedead;      // Flag whether or not monster was killed before.

//--------------//
//  PROTOTYPES  //
//--------------//
void(vector org) spawn_tfog;    // Found in 'triggers.qc'.
void() s_explode1;              // Found in 'weapons.qc'.

//==========================================================================

//-------------------------------------------------------- New Code --------
//  This checks if the spot is empty or occupied by a solid.
//--------------------------------------------------------------------------
float(vector spot) PM_Occupied =
{
		// If a wall or other solid is in the way, the monster can't respawn here.
    	if (PM_InWall(self, spot))  return TRUE;
		// Check if other obstacles such as entities are occupying the space.
		return !PM_TraceCheck(self, spot);
};

//-------------------------------------------------------- New Code --------
//  The creates a blank entity that is used for the explosion sprite
//  instead of the monster entity.
//--------------------------------------------------------------------------
void() PM_CoverUp =
{
// Freeze the monster in place and make it invisible.
		self.movetype   = MOVETYPE_NONE;
		self.solid	  = SOLID_NOT;
		self.velocity   = '0 0 0';
		self.touch	  = SUB_Null;
		setmodel (self, "");
// Create an explosion to cover up the monster.
		local   entity  blank;
		blank = spawn();
		setmodel (blank, "progs/s_explod.spr");
		setsize (blank, '0 0 0', '0 0 0');		
		setorigin (blank, self.origin);
		blank.movetype  = MOVETYPE_NONE;
		blank.solid	 = SOLID_NOT;
		blank.velocity  = '0 0 0';
		blank.touch	 = SUB_Null;
		blank.nextthink = time;
		blank.think	 = s_explode1;
};

//==========================================================================
//  RESET FUNCTIONS

//-------------------------------------------------------- New Code --------
//  A newly respawned monster either stays in one place or travels
//  the path it was following after the start of the level.
//--------------------------------------------------------------------------
void() PM_ResetPath =
{
	if (self.target) {
		self.goalentity = self.movetarget =
						  find(world, targetname, self.target);
		self.ideal_yaw = vectoyaw(self.goalentity.origin - self.origin);
		if (self.movetarget.classname == "path_corner")
			self.th_walk ();
		else {   self.pausetime = 99999999;
			self.th_stand ();
		}
	}
	else {   self.pausetime = 99999999;
			self.th_stand ();
	}
};

//-------------------------------------------------------- New Code --------
//  A newly respawned monster forgets all opponents who were not players.
//--------------------------------------------------------------------------
void() PM_ResetEnemy =
{
// If the enemy was a player, resume the attack.
		if (self.enemy.classname == "player") {
			if (self.oldenemy.classname != "player")
						self.oldenemy = world;
				HuntTarget ();	// FIXME:  Set goalentity.
				return;
		}
// If the next enemy was a player, attack him now.
		if (self.oldenemy.classname == "player") {
				self.enemy	 = self.oldenemy;
				self.oldenemy  = world;
				HuntTarget ();
				return;
		}
// There are no players to attack.
		self.enemy	 = world;
		self.oldenemy  = world;
		PM_ResetPath();
};

//-------------------------------------------------------- New Code --------
//  This reconstitutes the monster.
//--------------------------------------------------------------------------
void(vector hullmin, vector hullmax, float hp) PM_Reconstitute =
{
		self.solid	  = SOLID_SLIDEBOX;
		setsize (self, hullmin, hullmax);
		if (self.flags & FL_SWIM)
				self.view_ofs   = '0 0 10';
		else
				self.view_ofs   = '0 0 25';
		self.angles	 = self.oldangles;
		self.fixangle   = TRUE;
		self.flags	  = self.flags - (self.flags & FL_ONGROUND);
		self.health	 = hp;
		self.takedamage = DAMAGE_AIM;
};

//==========================================================================
//  RESPAWN TIME FUNCTIONS
//-------------------------------------------------------- New Code --------
//  This returns the amount of time in seconds that a monster will
//  remain incapacitated before its resurrection.  The value returned
//  ranges between 10 and 90.  (Similar to Nightmare skill in Doom.)
//
//  NOTE:  I do NOT like Quake's built-in random number generator.  The
//  numbers produced are biased toward the high end.  It took me some time
//  to produce times that are similar to Doom respawn times.  If the
//  code doesn't look as simple as it should, blame it on random(). :)
//--------------------------------------------------------------------------
float() PM_RandomSleep =
{
		local   float   sleeptime;	// How long monster stays down.
		local   float   d1, d2;	   // Random 'dice'.
		d1 = random();  d2 = random();
		sleeptime = (d1 + d2) * 45;
		if(cvar("mon_respawn_time"))
			sleeptime = (d1 + d2) * cvar("mon_respawn_time");
//		else
//		if (sleeptime < 10)  sleeptime = 1 + random()*10;
	   if (sleeptime < 2)  sleeptime = 1 + random()*10;
		return sleeptime;
};

//-------------------------------------------------------- New Code --------
//  This tells the monster to stay dead for 'sleeptime' seconds
//  before it revives.
//--------------------------------------------------------------------------
void(float sleeptime) PM_Sleep =
{
// If sleeptime given is too low, pick a random value.
		if (sleeptime < 5)  sleeptime = PM_RandomSleep();
		self.cnt		= 0;
		self.health	 = 0;
		self.nextthink  = time + sleeptime;
};

//==========================================================================
//  NIGHTMARE MODE FUNCTIONS

//-------------------------------------------------------- New Code --------
//  Find out if player is playing on Nightmare.
//--------------------------------------------------------------------------
float() PM_Nightmare =
{
		return (skill >= 3);
};

//-------------------------------------------------------- New Code --------
//  If the game is in respawn mode, revive slain monsters soon after
//  death.  Respawning is disabled in e1m7.
//--------------------------------------------------------------------------
float() PM_RespawnMode =
{
		if (PM_Nightmare()) {
			if (world.model == "maps/e1m7.bsp")
						return FALSE;   // Chthon -- don't respawn gibs!
				if (world.model == "maps/end.bsp") {
						local   entity  shub;
				// Shub-Niggurath -- respawn monsters only if Shub is alive.
						shub = find(world, classname, "monster_oldone");
						if (shub != world) {
							if (shub.health > 0)
										return TRUE;
						}
						return FALSE;
				}
				return TRUE;	// resurrect the monster
		}
		return FALSE;   // the monster is dead -- permanently
};

//--------------------------------------------------------------------------
//  END OF FILE.
//--------------------------------------------------------------------------
